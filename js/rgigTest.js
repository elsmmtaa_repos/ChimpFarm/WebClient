function postTask(){
    var query = {}
    query.name = $("#inputName").val();
    query.version = $("#inputVersion").val();
    query.apkPath = $("#inputAPK").val();
    query.apkPackage = $("#inputPackage").val();
    query.mainActivity = $("#inputActivity").val();
    query.eventCount = parseInt($("#inputEventC").val());
    query.osm = []
    $.each($(".inputDC"),function(index, value){
        if($(value).val()!==""&&$(value).val()!==0){
            var tempCont = {}
            var id = $(value).attr('id').split(";")
            tempCont.osv = id[0]
            tempCont.sr = id[1]
            tempCont.amount = $(value).val()
            query.osm.push(tempCont)
        }
    })
    query.eventTypeDistro = {}
    query.eventTypeDistro.touch = parseInt($("#inputToE").val())
    query.eventTypeDistro.nav = parseInt($("#inputNav").val())
    query.eventTypeDistro.motion = parseInt($("#inputMot").val())
    query.eventTypeDistro.trackball = parseInt($("#inputTrE").val())
    if(($("#inputToE").val()+$("#inputNav").val()+$("#inputMot").val()+$("#inputTrE").val()+$("#inputAnE").val())<100){
        query.eventTypeDistro.anyEvent = 100-($("#inputToE").val()+$("#inputNav").val()+$("#inputMot").val()+$("#inputTrE").val())
    }
    else {
        query.eventTypeDistro.anyEvent = parseInt($("#inputAnE").val())
    }
    console.log(JSON.stringify(query).replace(/\"/g,"\'"));
    $.ajax({
      url:serverURL+"/task",
      type:"POST",
      data:(JSON.stringify(query).replace(/\"/g,"\'")),
      dataType:"json",
      success: function(dataaa){
        console.log(dataaa)
        window.location = "index.html"
      }
    })
}
 
$("#inputToE").change( 
    function(){
        $("#tobar").css("width",$("#inputToE").val()+"%");
    }
);
$("#inputNav").change( 
    function(){
        $("#nbar").css("width",$("#inputNav").val()+"%");
    }
);
$("#inputMot").change( 
    function(){
        $("#mbar").css("width",$("#inputMot").val()+"%");
    }
);
$("#inputTrE").change( 
    function(){
        $("#trbar").css("width",$("#inputTrE").val()+"%");
    }
);
$("#inputAnE").change( 
    function(){
        $("#abar").css("width",$("#inputAnE").val()+"%");
    }
);