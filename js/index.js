window.onload = initPage
function initPage(){
	retrieveFTasks()
	retrieveQTasks()
}

function retrieveFTasks(){
    $.get(
	    serverURL+"/ftask",
	    {},
	    function(data) {
	    	$("#fnTasks").empty()
	    	for (var i = 0; i < data.length; i++) {
	    		var tr = document.createElement("tr")
	    		var id = document.createElement("td");
		        id.appendChild(document.createTextNode(i+1));
		        tr.appendChild(id);
		        var date = document.createElement("td")
		        date.appendChild(document.createTextNode((new Date(data[i].date)).toISOString()))
		        tr.appendChild(date);
		        var version = document.createElement("td")
		        version.appendChild(document.createTextNode(data[i].version))
		        tr.appendChild(version);
		        var name = document.createElement("td")
		        name.appendChild(document.createTextNode(data[i].name))
		        tr.appendChild(name);
		        var appPackage = document.createElement("td")
		        appPackage.appendChild(document.createTextNode(data[i].apkPackage))
		        tr.appendChild(appPackage);
		        var mainActivity = document.createElement("td")
		        mainActivity.appendChild(document.createTextNode(data[i].mainActivity))
		        tr.appendChild(mainActivity);
		        var osm = document.createElement("td")
		        var osmB = document.createElement("button")
		        osmB.setAttribute("type","button")
		        osmB.className = "btn btn-primary btn-sm"
		        osmB.setAttribute("data-toggle","modal")
		        osmB.setAttribute("data-target","#myModal")
		        osmB.appendChild(document.createTextNode("View OS Matrix"))
		        osm.appendChild(osmB)
		        tr.appendChild(osm)
		        var eventTypeDistro = document.createElement("td")
		        var eTDD = document.createElement("div")
		        eTDD.className = "progress"
		        var eTDtouch = document.createElement("div")
		        eTDtouch.className = "progress-bar progress-bar-success"
		        eTDtouch.setAttribute("style","width: "+data[i].eventTypeDistro.touch+"%")
		        eTDtouch.setAttribute("data-toggle","tooltip")
		        eTDtouch.setAttribute("data-placement","top")
		        eTDtouch.setAttribute("title",""+data[i].eventTypeDistro.touch+"%")
		        eTDtouch.appendChild(document.createTextNode("Touch"))
		        eTDD.appendChild(eTDtouch)
		        var eTDnav = document.createElement("div")
		        eTDnav.className = "progress-bar progress-bar-warning"
		        eTDnav.setAttribute("style","width: "+data[i].eventTypeDistro.nav+"%")
		        eTDnav.setAttribute("data-toggle","tooltip")
		        eTDnav.setAttribute("data-placement","top")
		        eTDnav.setAttribute("title",""+data[i].eventTypeDistro.nav+"%")
		        eTDnav.appendChild(document.createTextNode("Nav"))
		        eTDD.appendChild(eTDnav)
		        var eTDmotion = document.createElement("div")
		        eTDmotion.className = "progress-bar progress-bar-danger"
		        eTDmotion.setAttribute("style","width: "+data[i].eventTypeDistro.motion+"%")
		        eTDmotion.setAttribute("data-toggle","tooltip")
		        eTDmotion.setAttribute("data-placement","top")
		        eTDmotion.setAttribute("title",""+data[i].eventTypeDistro.motion+"%")
		        eTDmotion.appendChild(document.createTextNode("Motion"))
		        eTDD.appendChild(eTDmotion)
		        var eTDtrackball = document.createElement("div")
		        eTDtrackball.className = "progress-bar progress-bar-info"
		        eTDtrackball.setAttribute("style","width: "+data[i].eventTypeDistro.trackball+"%")
		        eTDtrackball.setAttribute("data-toggle","tooltip")
		        eTDtrackball.setAttribute("data-placement","top")
		        eTDtrackball.setAttribute("title",""+data[i].eventTypeDistro.trackball+"%")
		        eTDtrackball.appendChild(document.createTextNode("Trackball"))
		        eTDD.appendChild(eTDtrackball)
		        var eTDanyEvent = document.createElement("div")
		        eTDanyEvent.className = "progress-bar progress-bar-primary"
		        eTDanyEvent.setAttribute("style","width: "+data[i].eventTypeDistro.anyEvent+"%")
		        eTDanyEvent.setAttribute("data-toggle","tooltip")
		        eTDanyEvent.setAttribute("data-placement","top")
		        eTDanyEvent.setAttribute("title",""+data[i].eventTypeDistro.anyEvent+"%")
		        eTDanyEvent.appendChild(document.createTextNode("Any Event"))
		        eTDD.appendChild(eTDanyEvent)
		        eventTypeDistro.appendChild(eTDD)
		        tr.appendChild(eventTypeDistro)
		        var eventCount = document.createElement("td")
		        eventCount.appendChild(document.createTextNode(data[i].eventCount))
		        tr.appendChild(eventCount);
		        var actions = document.createElement("td")
		        var details = document.createElement("a")
		        details.setAttribute("href","resume.html?id="+data[i]._id)
		        var dIcon = document.createElement("span")
		        dIcon.className = "glyphicon glyphicon-search"
		        dIcon.setAttribute("aria-hidden","true")
		        details.appendChild(dIcon)
		        actions.appendChild(details)
		        var rerun = document.createElement("a")
		        rerun.setAttribute("href","rerun.html?id="+data[i]._id)
		        var rrIcon = document.createElement("span")
		        rrIcon.className = "glyphicon glyphicon-forward"
		        rrIcon.setAttribute("aria-hidden","true")
		        rerun.appendChild(rrIcon)
		        actions.appendChild(rerun)
		        var remove = document.createElement("a")
		        remove.setAttribute("href","deleteTask.html?id="+data[i]._id)
		        var rvIcon = document.createElement("span")
		        rvIcon.className = "glyphicon glyphicon-trash"
		        rvIcon.setAttribute("aria-hidden","true")
		        remove.appendChild(rvIcon)
		        actions.appendChild(remove)
		        tr.appendChild(actions)
		        $("#fnTasks").append(tr)
	    	};
	    }
	);
}

function retrieveQTasks(){
    $.get(
	    serverURL+"/cftask",
	    {},
	    function(data) {
	    	$("#iqTasks").empty()
	    	for (var i = 0; i < data.length; i++) {
	    		var tr = document.createElement("tr")
	    		var id = document.createElement("td");
		        id.appendChild(document.createTextNode(i+1));
		        tr.appendChild(id);
		        var date = document.createElement("td")
		        date.appendChild(document.createTextNode((new Date(data[i].date)).toISOString()))
		        tr.appendChild(date);
		        var version = document.createElement("td")
		        version.appendChild(document.createTextNode(data[i].version))
		        tr.appendChild(version);
		        var name = document.createElement("td")
		        name.appendChild(document.createTextNode(data[i].name))
		        tr.appendChild(name);
		        var appPackage = document.createElement("td")
		        appPackage.appendChild(document.createTextNode(data[i].apkPackage))
		        tr.appendChild(appPackage);
		        var mainActivity = document.createElement("td")
		        mainActivity.appendChild(document.createTextNode(data[i].mainActivity))
		        tr.appendChild(mainActivity);
		        var osm = document.createElement("td")
		        var osmB = document.createElement("button")
		        osmB.setAttribute("type","button")
		        osmB.className = "btn btn-info btn-sm"
		        osmB.setAttribute("data-toggle","modal")
		        osmB.setAttribute("data-target","#myModall")
		        osmB.appendChild(document.createTextNode("View OS Matrix"))
		        osm.appendChild(osmB)
		        tr.appendChild(osm)
		        var eventTypeDistro = document.createElement("td")
		        var eTDD = document.createElement("div")
		        eTDD.className = "progress"
		        var eTDtouch = document.createElement("div")
		        eTDtouch.className = "progress-bar progress-bar-success"
		        eTDtouch.setAttribute("style","width: "+data[i].eventTypeDistro.touch+"%")
		        eTDtouch.setAttribute("data-toggle","tooltip")
		        eTDtouch.setAttribute("data-placement","top")
		        eTDtouch.setAttribute("title",""+data[i].eventTypeDistro.touch+"%")
		        eTDtouch.appendChild(document.createTextNode("Touch"))
		        eTDD.appendChild(eTDtouch)
		        var eTDnav = document.createElement("div")
		        eTDnav.className = "progress-bar progress-bar-warning"
		        eTDnav.setAttribute("style","width: "+data[i].eventTypeDistro.nav+"%")
		        eTDnav.setAttribute("data-toggle","tooltip")
		        eTDnav.setAttribute("data-placement","top")
		        eTDnav.setAttribute("title",""+data[i].eventTypeDistro.nav+"%")
		        eTDnav.appendChild(document.createTextNode("Nav"))
		        eTDD.appendChild(eTDnav)
		        var eTDmotion = document.createElement("div")
		        eTDmotion.className = "progress-bar progress-bar-danger"
		        eTDmotion.setAttribute("style","width: "+data[i].eventTypeDistro.motion+"%")
		        eTDmotion.setAttribute("data-toggle","tooltip")
		        eTDmotion.setAttribute("data-placement","top")
		        eTDmotion.setAttribute("title",""+data[i].eventTypeDistro.motion+"%")
		        eTDmotion.appendChild(document.createTextNode("Motion"))
		        eTDD.appendChild(eTDmotion)
		        var eTDtrackball = document.createElement("div")
		        eTDtrackball.className = "progress-bar progress-bar-info"
		        eTDtrackball.setAttribute("style","width: "+data[i].eventTypeDistro.trackball+"%")
		        eTDtrackball.setAttribute("data-toggle","tooltip")
		        eTDtrackball.setAttribute("data-placement","top")
		        eTDtrackball.setAttribute("title",""+data[i].eventTypeDistro.trackball+"%")
		        eTDtrackball.appendChild(document.createTextNode("Trackball"))
		        eTDD.appendChild(eTDtrackball)
		        var eTDanyEvent = document.createElement("div")
		        eTDanyEvent.className = "progress-bar progress-bar-primary"
		        eTDanyEvent.setAttribute("style","width: "+data[i].eventTypeDistro.anyEvent+"%")
		        eTDanyEvent.setAttribute("data-toggle","tooltip")
		        eTDanyEvent.setAttribute("data-placement","top")
		        eTDanyEvent.setAttribute("title",""+data[i].eventTypeDistro.anyEvent+"%")
		        eTDanyEvent.appendChild(document.createTextNode("Any Event"))
		        eTDD.appendChild(eTDanyEvent)
		        eventTypeDistro.appendChild(eTDD)
		        tr.appendChild(eventTypeDistro)
		        var eventCount = document.createElement("td")
		        eventCount.appendChild(document.createTextNode(data[i].eventCount))
		        tr.appendChild(eventCount);
		        var sum = data[i].statuses.iq + data[i].statuses.pr + data[i].statuses.rn + data[i].statuses.fn
		        var arr = [((data[i].statuses.iq*100)/sum),((data[i].statuses.pr*100)/sum),((data[i].statuses.rn*100)/sum),((data[i].statuses.fn*100)/sum)]
		        var summ = arr[0]+arr[1]+arr[2]+arr[3]
		        if(summ!=100){
		        	arr[indexOfMax(arr)]=arr[indexOfMax(arr)]+(100-summ)
		        }
		        var statuses = document.createElement("td")
		        var sD = document.createElement("div")
		        sD.className = "progress"
		        var sIQ = document.createElement("div")
		        sIQ.className = "progress-bar progress-bar-danger progress-bar-striped active"
		        sIQ.setAttribute("style","width: "+arr[0]+"%")
		        sIQ.setAttribute("role","progressbar")
		        sIQ.setAttribute("aria-valuenow","45")
		        sIQ.setAttribute("aria-valuemin","0")
		        sIQ.setAttribute("aria-valuemax","100")
		        sIQ.setAttribute("data-toggle","tooltip")
		        sIQ.setAttribute("data-placement","top")
		        sIQ.setAttribute("title",""+arr[0]+"%")
		        sD.appendChild(sIQ)
		        var sPR = document.createElement("div")
		        sPR.className = "progress-bar progress-bar-warning progress-bar-striped active"
		        sPR.setAttribute("style","width: "+arr[1]+"%")
		        sPR.setAttribute("role","progressbar")
		        sPR.setAttribute("aria-valuenow","45")
		        sPR.setAttribute("aria-valuemin","0")
		        sPR.setAttribute("aria-valuemax","100")
		        sPR.setAttribute("data-toggle","tooltip")
		        sPR.setAttribute("data-placement","top")
		        sPR.setAttribute("title",""+arr[1]+"%")
		        sD.appendChild(sPR)
		        var sRN = document.createElement("div")
		        sRN.className = "progress-bar progress-bar-info progress-bar-striped active"
		        sRN.setAttribute("style","width: "+arr[2]+"%")
		        sRN.setAttribute("role","progressbar")
		        sRN.setAttribute("aria-valuenow","45")
		        sRN.setAttribute("aria-valuemin","0")
		        sRN.setAttribute("aria-valuemax","100")
		        sRN.setAttribute("data-toggle","tooltip")
		        sRN.setAttribute("data-placement","top")
		        sRN.setAttribute("title",""+arr[2]+"%")
		        sD.appendChild(sRN)
		        var sFN = document.createElement("div")
		        sFN.className = "progress-bar progress-bar-success progress-bar-striped active"
		        sFN.setAttribute("style","width: "+arr[3]+"%")
		        sFN.setAttribute("role","progressbar")
		        sFN.setAttribute("aria-valuenow","45")
		        sFN.setAttribute("aria-valuemin","0")
		        sFN.setAttribute("aria-valuemax","100")
		        sFN.setAttribute("data-toggle","tooltip")
		        sFN.setAttribute("data-placement","top")
		        sFN.setAttribute("title",""+arr[3]+"%")
		        sD.appendChild(sFN)
		        statuses.appendChild(sD)
		        tr.appendChild(statuses)
		        $("#iqTasks").append(tr)
	    	};
	    }
	);
}

function indexOfMax(arr) {
    if (arr.length === 0) {
        return -1;
    }

    var max = arr[0];
    var maxIndex = 0;

    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }

    return maxIndex;
}

function deleteTask(id){
	$.ajax({
	    url: serverURL+"/task/"+id,
	    type: 'DELETE',
	    success: function(data) {
			retrieveFTasks();        
	    }
	});

}