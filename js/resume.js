window.onload = initresume

function initresume(){
	var url = new URL(window.location.href);
	var id = url.searchParams.get("id");
	$.get(
		serverURL+"/task/"+id,
	    {},
	    function(data) {
	    	console.log(data)
	    	$("#inputName").val(data.task.name)
	    	$("#inputVersion").val(data.task.version)
	    	$("#inputAPK").val(data.task.apkPath)
	    	$("#inputPackage").val(data.task.apkPackage)
	    	$("#inputActivity").val(data.task.mainActivity)
	    	$("#inputEventC").val(data.task.eventCount)
	    	$("#inputToE").val(data.task.eventTypeDistro.touch)
	    	$("#inputNav").val(data.task.eventTypeDistro.nav)
	    	$("#inputMot").val(data.task.eventTypeDistro.motion)
	    	$("#inputTrE").val(data.task.eventTypeDistro.trackball)
	    	$("#inputAnE").val(data.task.eventTypeDistro.anyEvent)
	    	var to = data.task.eventTypeDistro.touch+"%"
	    	var nv = data.task.eventTypeDistro.nav+"%"
	    	var mt = data.task.eventTypeDistro.motion+"%"
	    	var tr = data.task.eventTypeDistro.trackball+"%"
	    	var ae = data.task.eventTypeDistro.anyEvent+"%"
	    	$("#tobar").css("width",to)
	    	$("#nbar").css("width",nv)
	    	$("#mbar").css("width",mt)
	    	$("#trbar").css("width",tr)
	    	$("#abar").css("width",ae)
	    	for (var i = 0; i < data.tasks.length; i++) {
	    		var tr = document.createElement("tr")
		    	var id = document.createElement("td");
			    id.appendChild(document.createTextNode(i+1));
			    tr.appendChild(id);
			    var osv = document.createElement("td")
			    osv.appendChild(document.createTextNode(data.tasks[i].osv))
			    tr.appendChild(osv);
			    var sr = document.createElement("td")
			    sr.appendChild(document.createTextNode(data.tasks[i].sr))
			    tr.appendChild(sr)
			    var status = document.createElement("td")
			    var stImage = document.createElement("img")
			    stImage.className = "chimpICO"
			    stImage.setAttribute("src","imgs/favicon.ico")
			    var stSpan = document.createElement("span")
			    if(data.tasks[i].result == "Pass"){
			    	stSpan.className = "badge pass-badge"
			    	stSpan.appendChild(document.createTextNode("Pass"))
			    }
			    else{
			    	stSpan.className = "badge crash-badge"
			    	stSpan.appendChild(document.createTextNode("Crash"))
			    }
			    status.appendChild(stImage)
			    status.appendChild(stSpan)
			    tr.appendChild(status)
			    var actions = document.createElement("td")
			    var elB = document.createElement("button")
			    elB.className = "btn btn-primary btn-xs"
			    elB.setAttribute("data-toggle","modal")
			    elB.setAttribute("data-target","#myyModal")
			    elB.appendChild(document.createTextNode("Execution Log"))
			    actions.appendChild(elB)
			    var lcB = document.createElement("button")
			    lcB.className = "btn btn-info btn-xs"
			    lcB.setAttribute("data-toggle","modal")
			    lcB.setAttribute("data-target","#myMModal")
			    lcB.appendChild(document.createTextNode("Logcat"))
			    actions.appendChild(lcB)
			    tr.appendChild(actions)
			    $("#resumeTable").append(tr)
	    	};
	    })
}
